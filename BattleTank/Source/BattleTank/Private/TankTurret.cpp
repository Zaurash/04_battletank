// Fill out your copyright notice in the Description page of Project Settings.

#include "../Public/TankTurret.h"
#include "Engine/World.h"

void UTankTurret::Rotate(float RelativeSpeed) {
	RelativeSpeed = FMath::Clamp<float>(RelativeSpeed, -2, 2);
	auto RotationChange = RelativeSpeed * MaxDegreesPerSecond * GetWorld()->DeltaTimeSeconds;
	auto Rotation = RelativeRotation.Yaw + RotationChange;
	//auto blah = RelativeRotation;

	SetRelativeRotation(FRotator(0, Rotation ,0));
}


