// Fill out your copyright notice in the Description page of Project Settings.

#include "../Public/TankAIController.h"
#include "Public/TankAimingComponent.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "Runtime/Engine/Classes/GameFramework/PlayerController.h"
#include "Tank.h" // So we can implement OnDeath
#include "BattleTank.h"



void ATankAIController::BeginPlay()
{
	Super::BeginPlay();

}

void ATankAIController::SetPawn(APawn * InPawn)
{
	Super::SetPawn(InPawn);

	if (InPawn) {
		auto PossessedTank = Cast<ATank>(InPawn);
		if (!ensure(PossessedTank)) { return; }

		// Subscribe local method to the tank's death event
		PossessedTank->OnDeath.AddUniqueDynamic(this, &ATankAIController::OnPossessedTankDeath);
	}
}

void ATankAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	APawn* PlayerTank = GetWorld()->GetFirstPlayerController()->GetPawn(); // Might not need?
	APawn* ControlledTank = GetPawn();
	UTankAimingComponent* AimComponent = ControlledTank->FindComponentByClass<UTankAimingComponent>();
	

	if (!ensure(PlayerTank && ControlledTank)) { return; }
		// TODO Move Towards Player
		MoveToActor(PlayerTank, AcceptanceRadius); // TODO check radius is in CM
		FVector PlayerLocation = PlayerTank->GetActorLocation(); // Possibly change to  GetFirstPlayerController()->GetTargetLocation()
		AimComponent->AimAt(PlayerLocation);

		// TODO Limit firing rate and FIX FIRING
		if (AimComponent->GetFiringState() == EFiringStatus::Locked) {
			AimComponent->Fire();
		}
}

void ATankAIController::OnPossessedTankDeath()
{
	if (!GetPawn()) { return; }
	GetPawn()->DetachFromControllerPendingDestroy();
}
