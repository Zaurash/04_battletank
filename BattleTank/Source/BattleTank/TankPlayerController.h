// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//#include "Tank.h"
#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TankPlayerController.generated.h" // Must be the last include

/**
 * Responsible for helping the player aim.
 */

class UTankAimingComponent;

UCLASS()
class BATTLETANK_API ATankPlayerController : public APlayerController
{
	GENERATED_BODY()
	

private:
	virtual void Tick(float DeltaTime) override;
	
	virtual void BeginPlay() override;

	void SetPawn(APawn * InPawn);

	UFUNCTION()
	void OnPossessedTankDeath();

	//Tells tank to aim at the crosshair
	void AimTowardsCrosshair();

	// Return an OUT parameter, true if it hits landscape
	bool GetSightRayHitLocation(FVector& OutHitLocation) const;

	bool GetLookDirection(FVector2D ScreenLocation, FVector & LookDirection) const;

	bool GetLookVectorHitLocation(FVector LookDirection, FVector& OutHitLocation) const;

	// Variables
	UPROPERTY(EditDefaultsOnly)
	float CrossHairXLocation = .5;
	UPROPERTY(EditDefaultsOnly)
	float CrossHairYLocation = .3333;
	UPROPERTY(EditDefaultsOnly)
	float LineTraceRange = 100000; // Change this to edit the raycast length for the tanks

protected:
	UFUNCTION(BlueprintImplementableEvent, Category = "Setup")
	void FoundAimingComponent(UTankAimingComponent* AimCompRef);
};
